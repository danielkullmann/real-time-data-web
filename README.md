Simple Javascript application for showing real-time data.

This app uses the jQuery and flot libraries to display real-time data in several graphs.

The data is test data, but the application has been used to display real-time
measurement data from an experimental power system. This is because the data is only
available inside the internal network of that power system.


